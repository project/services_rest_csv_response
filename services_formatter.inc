<?php
/**
 * @file
 * Provides CSV Formatter to Services.
 */

/**
 * Services CSV Formatter.
 *
 * @class
 * Add CSV Formatter.
 */
class ServicesCSVFormatter implements ServicesFormatterInterface {

  /**
   * Render data to the string.
   *
   * @param array $data
   *   Data to render.
   *
   * @return string
   *   Returns output.
   */
  public function render($data = array()) {
    if (empty($data)) {
      return '';
    }

    // Build header row.
    $header = array();
    foreach (reset($data) as $key => $value) {
      $header[] = $key;
    }
    array_unshift($data, $header);

    $handle = fopen('php://memory', 'w');
    foreach ($data as $row) {
      fputcsv($handle, $row, ';');
    }
    fseek($handle, 0);
    $output = stream_get_contents($handle);
    fclose($handle);

    return $output;
  }

}
